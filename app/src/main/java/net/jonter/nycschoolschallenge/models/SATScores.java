package net.jonter.nycschoolschallenge.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.Expose;

@Table(name="SATScores")
public class SATScores extends Model
{

    @Expose
    @Column(name="dbn", index=true)
    public String dbn;

    @Expose
    @Column(name="school_name")
    public String school_name;

    @Expose
    @Column(name="num_of_sat_test_takers")
    public String num_of_sat_test_takers;

    @Expose
    @Column(name="sat_critical_reading_avg_score")
    public String sat_critical_reading_avg_score;

    @Expose
    @Column(name="sat_math_avg_score")
    public String sat_math_avg_score;

    @Expose
    @Column(name="sat_writing_avg_score")
    public String sat_writing_avg_score;


    public SATScores()
    {

    }
}