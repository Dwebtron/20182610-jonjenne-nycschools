package net.jonter.nycschoolschallenge;


import net.jonter.nycschoolschallenge.models.SATScores;
import net.jonter.nycschoolschallenge.models.Schools;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiInterface
{

    @GET("97mf-9njv.json")
    Call<List<Schools>> getTopSchools();

    @GET("734v-jeq5.json")
    Call<List<SATScores>> getSATScores();
}
