package net.jonter.nycschoolschallenge;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.Select;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import net.jonter.nycschoolschallenge.models.SATScores;
import net.jonter.nycschoolschallenge.models.Schools;

import java.util.List;


public class SchoolDetail extends AppCompatActivity implements OnMapReadyCallback
{
    TextView schoolNameText;
    Schools school;
    SATScores score;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        ActiveAndroid.initialize(this);
        setContentView(R.layout.activity_school_detail);
        schoolNameText = findViewById(R.id.school_name_detail);
        school = (Schools) getIntent().getSerializableExtra("school");

        schoolNameText.setText(school.school_name);

        TextView schoolNumberData = findViewById(R.id.school_number_data);
        schoolNumberData.setText(school.phone_number);

        TextView schoolAddressData = findViewById(R.id.school_address_data);
        schoolAddressData.setText(school.primary_address_line_1);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        //sometimes, there isn't SAT data. Catch this!
        try
        {
            score = getSATScore(school.dbn);
            TextView testTakers = findViewById(R.id.school_test_takers_data);
            testTakers.setText(score.num_of_sat_test_takers);

            TextView readingScore = findViewById(R.id.school_reading_score_data);
            readingScore.setText(score.sat_critical_reading_avg_score);

            TextView writingScore = findViewById(R.id.school_writing_score_data);
            writingScore.setText(score.sat_writing_avg_score);

            TextView mathScore = findViewById(R.id.school_math_score_data);
            mathScore.setText(score.sat_math_avg_score);
        }
        catch(IndexOutOfBoundsException e)
        {
            TextView testTakers = findViewById(R.id.school_test_takers_data);
            testTakers.setText(getString(R.string.no_sat_scores_found));

            TextView readingScore = findViewById(R.id.school_reading_score_data);
            readingScore.setText(getString(R.string.no_sat_scores_found));

            TextView writingScore = findViewById(R.id.school_writing_score_data);
            writingScore.setText(getString(R.string.no_sat_scores_found));

            TextView mathScore = findViewById(R.id.school_math_score_data);
            mathScore.setText(getString(R.string.no_sat_scores_found));
        }
    }


    @Override
    public void onMapReady(GoogleMap map)
    {
        LatLng latlong = new LatLng(Double.parseDouble(school.latitude), Double.parseDouble(school.longitude));
        map.addMarker(new MarkerOptions()
                .position(latlong)
                .title(school.school_name)
                );

        //this isn't exactly centering the map on the marker, but this will have to do for now.
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(latlong, 15));
    }
    public static SATScores getSATScore(String dbn)
    {
        List<SATScores> scores = new Select()
                .from(SATScores.class)
                //we just want all of them, so...
                .where("dbn = ?", dbn)
                //.orderBy("school_name ASC")
                .execute();
        return scores.get(0);
    }
}
