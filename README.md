This is Jon Jenne's NYC Schools challenge project. My email is jon@jonter.net

I used the following depencies:
	* ActiveAndroid for easy database modeling and storage
	* GSON for easy API processing
	* RXJava for easy binding between Retrofit and ActiveAndroid
	* Retrofit 2 for easy API retrieval
	* okhttp for Retrofit to help with API retrieval
	* Google Play Services Map SDK to map out schools on an actual map


The app should work on any device starting at SDK 16 provided it has the Google Play Store / Google Play Services installed. It should also work without this as long as TLS v1.2 is available.

I've tested this on the following devices:

	Emulators:
	* Android SDK 28
	* Android SDK 27
	* Android SDK 24
	* Android SDK 16 (Fails due to no App Store/GPS installed on emulator)
	
	Real Devices:
	* Essential PH-1 SDK 28
	* Nexus 5X SDK 27


The UI isn't so spectacular, just the bare minimum to accomplish the goal. The MainActivity has 2 buttons, one for retreiving data from the API, and the other for displaying it in a list. I added a small picture of an Android with a graduation hat on to each row to spice this list up a bit. 
Once you click on a row item in the list, it will open the detail view, showing the school's name, phone number, address, SAT scores, SAT takers, as well as a map showing exactly where the school is located based on it's lat/long. 


I've tried to test manually for certain failure conditions, like no internet, no data, etc. I did not write any formal unit testing, since I'm a bit unfamiliar with that. The app should be smart enough to gracefully handle most error conditions you throw it's way. It should also not ask for any permisisons that have not been explicitly stated in the manifest; it's only permission is INTERNET. 

If you have any questions or any difficulty running the app, let me know at : jon@jonter.net and I'll be happy to help. 

